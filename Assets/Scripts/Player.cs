﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Player : NetworkBehaviour {

    public GameObject bullet;

    private float speed = 20.0f;
    private float accelerometerUpdateInterval = 1.0f / 60.0f;
    private float lowPassKernelWidthInSeconds = 0.5f;
    private float lowPassFilterFactor;
    private Vector3 lowPassValue;

	void Start () {
        lowPassFilterFactor = accelerometerUpdateInterval / lowPassKernelWidthInSeconds;
        lowPassValue = Vector3.zero;

        Transform firstCamera = GameObject.Find("First Camera").transform;
        Transform secondCamera = GameObject.Find("Second Camera").transform;

        if (isLocalPlayer)
        {
            if (isServer)
            {
                firstCamera.GetComponent<Camera>().enabled = true;
                secondCamera.GetComponent<Camera>().enabled = false;
                transform.SetParent(firstCamera, false);
                transform.position = new Vector3(firstCamera.position.x, firstCamera.position.y, 10f);

            }
            else
            {
                firstCamera.GetComponent<Camera>().enabled = false;
                secondCamera.GetComponent<Camera>().enabled = true;
                transform.SetParent(secondCamera, false);
                transform.position = new Vector3(secondCamera.position.x, secondCamera.position.y, 10f);
            }
        }
    }
	
	void Update () {
        if(!isLocalPlayer)
        {
            return;
        }

        Vector3 direction = LowPassFilterAccelerometer();
        direction.z = 0f;

        if(direction.sqrMagnitude > 1)
        {
            direction.Normalize();
        }

        transform.Translate(direction * Time.deltaTime * speed);

        HandleTouchInput();
	}

    private Vector3 LowPassFilterAccelerometer()
    {
        lowPassValue = Vector3.Lerp(lowPassValue, Input.acceleration, lowPassFilterFactor);
        return lowPassValue;
    }

    private void HandleTouchInput()
    {
        foreach(Touch touch in Input.touches)
        {
            if(touch.phase == TouchPhase.Began)
            {
                int direction;
                if (isServer)
                {
                    direction = 1;
                }
                else
                {
                    direction = -1;
                }
                CmdShootBullet(direction);
            }
        }
    }

    [Command]
    private void CmdShootBullet(int direction)
    {
        GameObject firedBullet = (GameObject)Instantiate(bullet, transform.position, Quaternion.identity);
        Bullet bulletScript = firedBullet.GetComponent<Bullet>();
        bulletScript.SetDirection(direction);
        NetworkServer.Spawn(firedBullet);
    }
}
