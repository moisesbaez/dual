﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    public float speed = 10f;

    private int direction;

	void Start () {
	
	}
	
	void Update () {
        transform.position += new Vector3(0f, direction * speed * Time.deltaTime, 0f);
	}

    public void SetDirection(int direction)
    {
        direction = (int)Mathf.Sign(direction);
        this.direction = direction;
    }
}
